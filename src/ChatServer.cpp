
#ifdef LINUX
#include <gtk/gtk.h>
#include <stdio.h>
#include <arpa/inet.h>  // inet_ntoa
#include <pthread.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/time.h>
#endif //LINUX

#include "STARX/STARX_API.h"


char sendbuf[1024],getbuf[1024],username[64]="server";
gboolean isconnected = FALSE;

GtkWidget *text;
GtkTextBuffer *buffer;
GtkWidget *message_entry;
GtkWidget *listenbutton;
GtkWidget *closebutton;

GMutex data_mutex; /* Must be initialized somewhere */

INT32 sessionHandle;

gchar *active_did;
GtkTextIter start, end;


void log(char *msg, int len) {
	// g_mutex_lock (&data_mutex);

	gtk_text_buffer_insert(buffer,&end,msg,len);
    // gtk_text_buffer_delete (buffer,&start,&end);

	// g_mutex_unlock (&data_mutex);
}

void get_message(void)
{
	memset(&getbuf,0,sizeof(getbuf));

	UINT32 APIVersion = STARX_GetAPIVersion();
	printf("STARX_API Version: %d.%d.%d.%d\n", (APIVersion & 0xFF000000) >> 24,
		(APIVersion & 0x00FF0000) >> 16, (APIVersion & 0x0000FF00) >> 8,
		(APIVersion & 0x000000FF) >> 0);

	INT32 ret;
	ret =
	STARX_Initialize(
		(CHAR*) "EFGBFFBJKFJOGCJNFHHCFHEMGENHHBMHHLFGBKDFAMJLLDKHDHACDEPBGCLAIALDADMPKDDIODMEBOCNJLNDJJ");

	st_STARX_NetInfo NetInfo;
	ret = STARX_NetworkDetect(&NetInfo, 0);

	printf("-------------- NetInfo: -------------------\n");
	printf("Internet Reachable     : %s\n",
		(NetInfo.bFlagInternet == 1) ? "YES" : "NO");
	printf("P2P Server IP resolved : %s\n",
		(NetInfo.bFlagHostResolved == 1) ? "YES" : "NO");
	printf("P2P Server Hello Ack   : %s\n",
		(NetInfo.bFlagServerHello == 1) ? "YES" : "NO");
	printf("Local NAT Type         :");
	STARX_Share_Bandwidth(1);
	switch (NetInfo.NAT_Type) {
		case 0:
		printf(" Unknow\n");
		break;
		case 1:
		printf(" IP-Restricted Cone\n");
		break;
		case 2:
		printf(" Port-Restricted Cone\n");
		break;
		case 3:
		printf(" Symmetric\n");
		break;
	}
	printf("My Wan IP : %s\n", NetInfo.MyWanIP);
	printf("My Lan IP : %s\n", NetInfo.MyLanIP);

	while(1) {
		log("Start to Listen...\n", -1);

		sessionHandle = STARX_Listen(active_did, 600, 0, (char)1);

		if(sessionHandle >= 0) {
			log("有新的客户端接入!\n",-1);
			isconnected=TRUE;

			while (true)
			{
				int ReadDataSize = 4;
				char *lenBuf = (char*)malloc(ReadDataSize);
				int ret = 0;

				ret = STARX_Read(sessionHandle, 1, lenBuf, &ReadDataSize, 60 * 1000); //60sec
				if (ret == ERROR_STARX_SESSION_CLOSED_TIMEOUT) {
					printf(
						"TransferFileDemo_Device, Session TimeOUT!!\n");
					break;
				} else if (ret == ERROR_STARX_SESSION_CLOSED_REMOTE) {
					printf(
						"TransferFileDemo_Device, Session Remote Close!!\n");
					break;
				} else if (ret == ERROR_STARX_INVALID_SESSION_HANDLE) {
					printf(
						"TransferFileDemo_Device, invalid session handle!!\n");
					break;
				}

				int len = (int)lenBuf[0];

				if (len > 0) {
					char *dataBuf = (char*)malloc(len);
					ret = STARX_Read(sessionHandle, 1, dataBuf, &len, 60 * 1000); //60sec

					log("收到: ", -1);
					log(dataBuf, len);
					log("\n", -1);
					free(dataBuf);
				}
				free(lenBuf);

				usleep(10 * 1000); //10ms
			}

		}
		usleep(1000 * 1000); //1sec
	}
}
void listen_client(void)
{

    gtk_text_buffer_get_start_iter (buffer,&start);
    gtk_text_buffer_get_end_iter (buffer,&end);

	g_thread_create((GThreadFunc)get_message,NULL,FALSE,NULL);
	//这里要配发新进程，不然直接阻塞，窗口卡死
}
void listenbutton_clicked(GtkButton *button,gpointer data)
{
	gtk_widget_set_sensitive(listenbutton,FALSE);//开启按钮失效
	g_thread_create((GThreadFunc)listen_client,NULL,FALSE,NULL);
}

void on_delete_event(GtkWidget *widget,GdkEvent *event,gpointer data)
{
	if(sessionHandle >= 0) {
		STARX_Close(sessionHandle);
	}
	STARX_DeInitialize();
	gtk_main_quit();
}

void on_send (GtkButton * button,gpointer data)
{
	char *message;
	
	printf("%s %s, LINE: %d\n", __FILE__, __FUNCTION__, __LINE__);
	if(isconnected==FALSE)  return;
	message=(char*)(gtk_entry_get_text(GTK_ENTRY(message_entry)));

	int dataLen = strlen(message);

	memset(&sendbuf,0,sizeof(sendbuf));
	memset(&sendbuf, dataLen, 1);
	memcpy((char*)(sendbuf+4), message, dataLen);
	printf("dataLen: %d\n", dataLen);
	int ret = STARX_Write(sessionHandle, 1, sendbuf, dataLen + 4);
	if (ret == ERROR_STARX_SESSION_CLOSED_TIMEOUT) {
		printf(
			"TransferFileDemo_Device, Session TimeOUT!!\n");
	} else if (ret == ERROR_STARX_SESSION_CLOSED_REMOTE) {
		printf(
			"TransferFileDemo_Device, Session Remote Close!!\n");
	} else if (ret == ERROR_STARX_INVALID_SESSION_HANDLE) {
		printf(
			"TransferFileDemo_Device, invalid session handle!!\n");
	}
				
	log("发送:",-1);
	log(message,-1);
	log("\n",-1);

}


gboolean did_combox_changed(GtkComboBox *comboBox, GtkLabel *label) {
	active_did = gtk_combo_box_get_active_text(comboBox);
}

int main (int argc, char *argv[])
{
	GtkWidget *window;
	GtkWidget *did_hbox;
	GtkWidget *did_label;
	GtkWidget *did_combox;
	GtkWidget *vbox,*hbox,*label,*view,*button;


	if(!g_thread_supported())
		g_thread_init(NULL);
	gtk_init(&argc,&argv);

	active_did = "STAR-000004-DRJJH";

	window =gtk_window_new(GTK_WINDOW_TOPLEVEL);
	gtk_window_set_title(GTK_WINDOW(window),"服务器");
	gtk_window_set_default_size(GTK_WINDOW(window), 640, 480);

	g_signal_connect(G_OBJECT(window),"delete_event",G_CALLBACK(on_delete_event),NULL);

	gtk_container_set_border_width(GTK_CONTAINER(window),10);
	vbox=gtk_vbox_new(FALSE,0);
	gtk_container_add(GTK_CONTAINER(window),vbox);

	did_hbox=gtk_hbox_new(FALSE,0);
	did_label = gtk_label_new("Device ID:");
	did_combox = gtk_combo_box_new_text();
	gtk_combo_box_append_text(GTK_COMBO_BOX(did_combox), "STAR-000004-DRJJH");
	gtk_combo_box_append_text(GTK_COMBO_BOX(did_combox), "momor");
	gtk_combo_box_append_text(GTK_COMBO_BOX(did_combox), "hamimi");
	gtk_combo_box_append_text(GTK_COMBO_BOX(did_combox), "bush");
	gtk_combo_box_set_active(GTK_COMBO_BOX(did_combox), 0);
	gtk_box_pack_start(GTK_BOX(did_hbox),did_label,FALSE,FALSE,5);
	gtk_box_pack_start(GTK_BOX(did_hbox),did_combox,FALSE,FALSE,5);

	g_signal_connect(GTK_OBJECT(did_combox), "changed", 
		G_CALLBACK(did_combox_changed), label);

	gtk_box_pack_start(GTK_BOX(vbox),did_hbox,FALSE,FALSE,5);

	hbox=gtk_hbox_new(FALSE,0);
	gtk_box_pack_start(GTK_BOX(vbox),hbox,FALSE,FALSE,5);
	listenbutton=gtk_button_new_with_label("Listen");
	gtk_box_pack_start(GTK_BOX(hbox),listenbutton,FALSE,FALSE,5);
	g_signal_connect(G_OBJECT(listenbutton),"clicked",G_CALLBACK(listenbutton_clicked),NULL);

	closebutton=gtk_button_new_with_label("Close");
	gtk_box_pack_start(GTK_BOX(hbox),closebutton,FALSE,FALSE,5);
	g_signal_connect(G_OBJECT(closebutton),"clicked",G_CALLBACK(on_delete_event),NULL);
	view=gtk_scrolled_window_new(NULL,NULL);
	gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(view),GTK_POLICY_AUTOMATIC,GTK_POLICY_AUTOMATIC);
	text=gtk_text_view_new();
	gtk_box_pack_start(GTK_BOX(vbox),view,TRUE,TRUE,5);
	gtk_container_add(GTK_CONTAINER(view),text);
	buffer=gtk_text_view_get_buffer(GTK_TEXT_VIEW(text));

	hbox=gtk_hbox_new(FALSE,0);
	gtk_box_pack_start(GTK_BOX(vbox),hbox,FALSE,FALSE,5);

	label=gtk_label_new("输入消息：");
	gtk_box_pack_start(GTK_BOX(hbox),label,FALSE,FALSE,5);

	message_entry=gtk_entry_new();
	gtk_box_pack_start(GTK_BOX(hbox),message_entry,FALSE,FALSE,5);

	button=gtk_button_new_with_label("发送");
	gtk_box_pack_start(GTK_BOX(hbox),button,FALSE,FALSE,5);
	g_signal_connect(G_OBJECT(button),"clicked",G_CALLBACK(on_send),NULL);

	gtk_widget_show_all(window);

	gdk_threads_enter();

	gtk_main();

	gdk_threads_leave();

	return FALSE;
}
