DEV_DID=STAR-000105-HPTLB
CFLAGS=-g -O2 -DLINUX -Wdeprecated-declarations -Wwrite-strings -w

#CXX=~/buildroot-gcc342/bin/mipsel-linux-g++
#ARCH=mips

CC=gcc
ARCH=x86

all: 
	$(CC) $(CFLAGS) -Iinclude src/ChatServer.cpp lib/$(ARCH)/libPPPP_API.so -o ChatServer -s -lpthread `pkg-config --cflags --libs gtk+-2.0`
test_ChatServer: all
	./ChatServer $(DEV_DID)
	

clean:
	rm -rf *.o *~ *.bak ChatServer

